#' @title Criterion A reduction in area
#' @name A_reduction_in_area
#' @param area_t1 the area in square units of the earlier timepoint
#' @param area_t2 the area in square units of the later timepoint
#' @param thresholds the percentages that distinguish CR, EN, VU or LC in order
#' @return one of CR, EN, VU or LC

A_reduction_in_area <- function(area_t1, area_t2, thresholds = c(80,50,30)){
area_decline  <- ((area_t1 - area_t2) / area_t1) * 100
# use criterion decision rules
if(area_decline >= thresholds[1]){
  result <- "CR" # Critically Endangered
 } else if (area_decline >= thresholds[2]){
  result <- "EN"  # equates to Endangered
 } else if (area_decline >= thresholds[3]){
  result <- "VU" # equates to Vulnerable.
} else {
  result <- "LC"
}
return(result)
}
