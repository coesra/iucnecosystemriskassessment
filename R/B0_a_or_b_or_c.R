
#' @title Subcriteria B1 or B2 subsubcriteria A or B or C
#' @param subcriterion either B1 or B2
#' @param a an observed or inferred continuing decline (Yes or No)
#'
#' @param b observed or inferred threatening processes that are likely to cause continuing declines
#' @param subcriterion either B1 or B2
#' @param c Ecosystem exists at N locations
#'
#' @return a data frame with the values for any (or all) subsubcriteria
B0_a_or_b_or_c  <- function(subcriterion = "B1", a = NA, b = NA, c = NA){
  
  B0_a_or_b_or_c  <- matrix(NA, ncol = 2, nrow = 0)

  if(!is.na(a)){
    if(!toupper(a) %in% c('YES', 'NO')) stop("subsubcriterion A should be Yes or No")
    B0_a_or_b_or_c <- rbind(B0_a_or_b_or_c, c(paste(subcriterion,"a", sep = "_"), a))
  }
   
  if(!is.na(b)){
    if(!toupper(b) %in% c('YES', 'NO')) stop("subsubcriterion B should be Yes or No")  
    B0_a_or_b_or_c <- rbind(B0_a_or_b_or_c, c(paste(subcriterion,"b", sep = "_"), b))
  }
   
  if(!is.na(c)){
    if(!is.numeric(c)) stop("subsubcriterion C should be numeric")  
    B0_a_or_b_or_c <- rbind(B0_a_or_b_or_c, c(paste(subcriterion,"c", sep = "_"), c))
  }
  B0_a_or_b_or_c  <- as.data.frame(B0_a_or_b_or_c)    
  names(B0_a_or_b_or_c)  <- c("subsubcriterion", "value")
  return(B0_a_or_b_or_c)
}
