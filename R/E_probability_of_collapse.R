#' @title Probability of collapse
#' @param prob probability
#' @param year year in future
#' @return result CR, EN, VU or LC
E_probability_of_collapse <- function(prob = NA, year=NA){
if(is.na(prob)) stop("no prob given")
# use criterion decision rules
if(year == 50){
  if(prob >= 50){
    result <- "CR" # Critically Endangered
  } else if (prob >= 20){
    result <- "EN"  # equates to Endangered
  }
} else if (year == 100) {
  if(prob >= 10){
    result <- "VU" # equates to Vulnerable.
} else {
    result <- "LC"
}
}
return(result)

}
