title: "IUCNEcosystemRiskAssessment"
---

To install this you need to have the `devtools` package from CRAN then run this R code:
   
    library(devtools)
    install_bitbucket("coesra/iucnecosystemriskassessment")
    
   
